function updatePrice() {
let s = document.getElementsByName("prodType");
let select = s[0];
let price = 0; 
let prices = getPrices();
let priceIndex = parseInt(select.value) - 1;
if (priceIndex >= 0) {
price = prices.prodTypes[priceIndex];
}



let radioDiv = document.getElementById("radios");
if (select.value == "1") {
radioDiv.style.display = "none";
}
else {
radioDiv.style.display = "block";
}
let radios = document.getElementsByName("prodOptions");
radios.forEach(function(radio) {
if (radio.checked) {
let optionPrice = prices.prodOptions[radio.value];
if (optionPrice !== undefined) {
price += optionPrice;
}
}
});

let checkDiv = document.getElementById("checkboxes");
if (select.value == "1" || select.value == "2") {
checkDiv.style.display = "none";
}
else if(select.value == "3") {
checkDiv.style.display = "block";
}
let checkboxes = document.querySelectorAll("#checkboxes input");
checkboxes.forEach(function(checkbox) {
if (checkbox.checked) {
let propPrice = prices.prodProperties[checkbox.name];
if (propPrice !== undefined) {
price += propPrice;
}
}
});

let f1 = document.getElementsByName("field1");
f1.forEach(function(text) {

let countPrice = f1[0].value;
let prodPrice = document.getElementById("prodPrice");
price = price * countPrice ;
prodPrice.innerHTML = price + " Рублей";
});

}

function getPrices() {
return {
prodTypes: [15000,25000 , 40000],
prodOptions: {
option1: 10000,
option2: 5000,
option3: 0,
},
prodProperties: {
prop1: 2000,
prop2: 1000,
}
};
}

window.addEventListener('DOMContentLoaded', function (event) {

let radioDiv = document.getElementById("radios");
radioDiv.style.display = "none";

let s = document.getElementsByName("prodType");
let select = s[0];
select.addEventListener("change", function(event) {
let target = event.target;
console.log(target.value);
updatePrice();
});

let radios = document.getElementsByName("prodOptions");
radios.forEach(function(radio) {
radio.addEventListener("change", function(event) {
let r = event.target;
console.log(r.value);
updatePrice();
});
});

let f1 = document.getElementsByName("field1");
f1.forEach(function(text) {
text.addEventListener("change", function(event) {
let t = event.target;
console.log(t.value);
updatePrice();

});
});

let checkboxes = document.querySelectorAll("#checkboxes input");
checkboxes.forEach(function(checkbox) {
checkbox.addEventListener("change", function(event) {
let c = event.target;
console.log(c.name);
console.log(c.value);
updatePrice();
});
});

updatePrice();
});